 "use strict";

/*  Теоретичні питання: ================================

Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
------------------------------------------------------

Цикл - це снеціальна конструкція, яка дозвляє в JS 
виконувати якусь частину коду якусь кількість разів.

//---------------------------------------------------
Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
-----------------------------------------------------

//  WHILE loop:

//  коли потрібно знайти числа, кратні "2" на додавання:

// let i = 0;

// while (i < 10) {
//   console.log(i);
//   alert(i);
//   i += 2;  
// }
--

// DO...WHILE loop:

// коли потрібно знайти числа, кратні "3" на віднімання: 

// let g = 15;

// do {
//   console.log(g);
//   alert(g);
//   g -= 3;

// } while (g >= -3);
--

//  FOR loop:

Коли потрібно було збудувати фігуру, маючи її параметри ширини та висоти:

let hight = Number(prompt("Enter hight"));
let width = Number(prompt("Enter width"));


let line = "";
let figure = "";

for (let i = 0; i < width; i++) {
  line += "*";
}

for (let i = 0; i < hight; i++) {
  figure += line + "\n";
}

console.log(figure);
alert(figure);

//---------------------------------------------------
Що таке явне та неявне приведення (перетворення) типів даних у JS?
-----------------------------------------------------

 Явне - це коли ми самі перетворюємо типи данних до одного значення:

    let firstNumber = Number(prompt("Enter first number:"));
    let secondtNumber = Number(prompt("Enter second number:"));

 - перетворення 'string' -> Number  відбулося завдяки 'Number', 
 прописанного перед значенням змінної.
--

 Неявне - коли сама JS перетворює типи данних, 
 створюючи якісь розрахунки, дії (і т.п.), та приводить до одного типу:

    let perimetr = (a + b) * 2;

-  спочатку відбулося "склеювання" строчних елементів (a + b), а потім, 
після подальшого їх помноження, відбулося перетворення 'string' -> Number.


// Завдання: ============================================ */


let number = 0;
number = prompt("Enter any number");

let numeric = Number(number);
 alert(numeric);
 console.log(numeric); 


while (numeric >= 5) {
  console.log(numeric -= 5);
  // numeric -= 5;
}
  if (numeric < 5) {
    console.log("Sorry, no numbers");
  }
  else  if (numeric == null) {
      alert("OK!");
      console.log(numeric);
      // break;
  }
 
//========================================================

// Необов'язкове завдання підвищеної складності:

// -------------------------------------------------------
/* Перевірити, чи введене значення є цілим числом. 
Якщо ця умова не дотримується, повторювати виведення 
вікна на екран, доки не буде введено ціле число.
---------------------------------------------------------- */

    while (isNaN(numeric)) {
      number = prompt("Incorrect number");
      numeric = Number(number);

      console.log(numeric);
      alert(numeric);
    }

    while (numeric >= 5) {
        console.log(numeric -= 5);

        if (numeric < 5) {
          console.log("Sorry, no numbers");
        }    

        else if (numeric == null) {
          alert("OK!");
          console.log(numeric);
          // break;
        }        
    }   


  /* ----------------------------------------------------
  Отримати два числа, m і n. Вивести в консоль усі прості числа 
  (http://uk.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) 
  в діапазоні від m до n (менше із введених чисел буде m, більше буде n). 
  Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище, 
  вивести повідомлення про помилку і запитати обидва числа знову.
  ------------------------------------------------------- */


  let m = Number(prompt("Enter first number:"));
  console.log(m);

  let n = Number(prompt("Enter second number greater than the first:"));
  console.log(n);


  while(m >= n) {
    n = Number(prompt("Error! №1 cannot be more than №2", "Try again:")); 
    // alert(n);
    console.log(n);
    }

    if (n == null) {
      alert("OK!");
      // console.log(n);
      // break;
    }

    else if (m == null) {
      alert("OK!");
      // console.log(m);
      // break;

    if(n > m) {
      n = n--;
      console.log(n--);  
    }
  }  
 
  while (isNaN(n)) {
    n = Number(prompt("Error! Incorrect second number:"));
    m = Number(prompt("Enter first number:"));
    console.log(n);
    console.log(m);
    // alert(n);

    if(m >= n) {
      n = Number(prompt("Error! №1 cannot be more than №2", "Try again:")); 
      alert(n);
      console.log(n);
    } 
  
    else if (n == null) {
      alert("OK!");
      console.log(n);
      break;
    }
  
    else if (m == null) {
      alert("OK!");
      console.log(m);
      break;
    }  

    else if (n > m) {
      n = n--;
       console.log(n--);  
    }
  }
  
  while (isNaN(m)) {
    m = Number(prompt("Error! Incorrect first number:"));
    n = Number(prompt("Enter second number greater than the first:"));
    console.log(m);
    console.log(n);
    // alert(m);

    if(m >= n) {
      m = Number(prompt("Error! №1 cannot be more than №2", "Try again:")); 
      alert(n);
      console.log(n);
    } 
  
    else if (m == null) {
      alert("OK!");
      console.log(m);
      break;
    }
  
    else if (n == null) {
      alert("OK!");
      console.log(n);
      break;
    } 

    else if(n > m) {
      n = n--;
      console.log(n--);  
    }
  }  

  while (n > m) {
   n = n--;
   console.log(n--);  
  }

//========================================================

// НЕ ВІДКРИВАТИ!!!

  // let m;
  // let n;
  // let o = Number(m >= n);
  // let p = Number(n > m);
  // let p = Number(--n);